# kops

A set of sample kops scripts

- Create aws account and generate Access Key Id and Secret
- Install awscli
- Run `aws configure` and provide the details
- Source `aws.config` to expose the environment variables
- Run `kops.aws` to give access to kops group and user
- Run `cluster.state.s3.aws` to create S3 bucket
- Source `kench.cluster.config` to prepare kops cluster
- Run `cluster.keys.aws` to create keys from existing public key. Make sure you have the private key.
- Finally, run `cluster.aws` for a simple cluster or `cluster.ha.aws` for more detailed cluster
